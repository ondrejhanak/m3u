//
//  ViewController.swift
//  m3u
//
//  Created by Ondrej Hanak on 17/02/2017.
//  Copyright © 2017 Ondrej Hanak. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()

		let url = Bundle.main.url(forResource: "playlist.m3u", withExtension: nil)
		let content = try! String(contentsOf: url!)

		let pattern = "#EXTINF:(-?\\d+)([^,]*),(.+)\n+(.+)"
		let regExp = try! RegExp(pattern: pattern, string: content)

		var channels = [Channel]()

		for item in regExp.matches {
			guard item.count == 5 else {
				continue
			}

			// params
			let regExp = try! RegExp(pattern: "(\\S+)=\"(.+?)\"", string: item[2])
			var params: [String: String] = [:]
			for pair in regExp.matches {
				let key = pair[1]
				let value = pair[2]
				params[key] = value
			}

			// name
			let name = item[3]

			// URL
			guard let url = URL(string: item[4]) else {
				return
			}

			let channel = Channel(name: name, url: url, params: params)
			channels.append(channel)
		}

	}

}

