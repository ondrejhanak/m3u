//
//  Channel.swift
//  m3u
//
//  Created by Ondřej Hanák on 18. 02. 2017.
//  Copyright © 2017 Ondrej Hanak. All rights reserved.
//

import Foundation

struct Channel {
	let name: String
	let url: URL
	let params: [String: String]
}
