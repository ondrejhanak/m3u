//
//  RegExp.swift
//  m3u
//
//  Created by Ondrej Hanak on 20/02/2017.
//  Copyright © 2017 Ondrej Hanak. All rights reserved.
//

import Foundation

final class RegExp {

	public private(set) var matches: [[String]] = []

	init(pattern: String, string: String, options: NSRegularExpression.Options = []) throws {
		let expression = try NSRegularExpression(pattern: pattern, options: options)
		let contentRange = NSRange(location: 0, length: string.characters.count)
		let matches = expression.matches(in: string, options: [], range: contentRange)

		var result = [[String]]()
		let nsstring = string as NSString
		for match in matches {
			var item = [String]()
			for i in 0 ..< match.numberOfRanges {
				let range = match.rangeAt(i)
				let substring = nsstring.substring(with: range)
				item.append(substring)
			}
			result.append(item)
		}
		self.matches = result
	}

}
